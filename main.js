function сreateNewUser() {
  let firstName = prompt("Enter your first name", "");
  console.log("First name:", firstName);
  let lastName = prompt("Enter your last name", "");
  console.log("Last name:", lastName);
  let birthDay = prompt("Enter your date of birth in the format: dd.mm.yyyy ", "dd.mm.yyyy");
  console.log("Date of birth:", birthDay);

  let birthDayDate = new Date(
    birthDay.slice(6, 10),
    birthDay.slice(3, 5) - 1,
    birthDay.slice(0, 2)
  );

  let newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin(firstName, lastName) {
      return (this.firstName[0].toLowerCase() + this.lastName.toLowerCase());
    },
    setNewFirstName(newFName) {
      Object.defineProperty(newUser, "firstName", {
        value: newFName,
      });
    },
    setNewLastName(newLName) {
      Object.defineProperty(newUser, "lastName", {
        value: newLName,
      });
    },
    getAge() {
      let nowDate = new Date();
      let age = (nowDate - birthDayDate) / (60 * 60 * 24 * 1000 * 365);
      return (`The result of the work getAge():====> Full age ${this.firstName} ${this.lastName}:  ${Math.floor(age)} years`);
    },
    getPassword() {
      return "The result of the work getPassword():====>  " + this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthDayDate.getFullYear();
    },
  };

  Object.defineProperties(newUser, {
    firstName: {
      writable: false,
      configurable: true,
    },
    lastName: {
      writable: false,
      configurable: true,
    },
  });

  return newUser;
}

let myUser = сreateNewUser();
console.log("Created object: ", myUser);

console.log(myUser.getAge());
console.log(myUser.getPassword());